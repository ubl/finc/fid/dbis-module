# VuFind DBIS Module

Introduction
------------

This module is an Open Source Software of the University Library Leipzig.
The module retrieve the data of [Datenbank-Infosystem (DBIS)](https://dbis.uni-regensburg.de//fachliste.php?lett=l).

Installation
------------

**Composer**

Packagist Repo [Module for DBIS Webservice](https://packagist.org/packages/finc/dbis-module)

To Install use Composer
    
    php composer require finc/dbis-module
    
**VuFind**

*theme.config.php*

Write to the configuration a *mixins*-statement.

```
return [
  [...],
  'mixins' => [
    'finc-dbis'
  ],
  [...]
];
```

*templates/**file**.phtml*

To display the data when write the following JS-Script in to a PHTML-File

```
<? $script = <<<JS
$(document).ready(function() {
  var recordId = $('.hiddenId').val();
  var recordSource = $('.hiddenSource').val();
  $.ajax({
    dataType: 'json',
    url: VuFind.path + '/AJAX/JSON?method=getDbis',
    method: 'GET',
    data: {id: recordId, source: recordSource}
  }).done(function(response) {
    $('.dbis-data').html(response.data.html);
  });
});
JS;
?>
<?=$this->inlineScript(\Laminas\View\Helper\HeadScript::SCRIPT, $script, 'SET');?>
<div class="dbis-data"></div>
```

Configuration
-------------

Create a configuration file in config folder.

**dbis.ini**
    
    ; Which subject links do you want to retrieve? 
    [Subject]
    value = 53
