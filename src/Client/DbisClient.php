<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 *
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Dbis\Client;

use finc\Dbis\Model\Api\DbisPage;
use finc\SymfonySerializerZendBridge\Normalizer\SnakeCaseObjectNormalizer;
use finc\SymfonySerializerZendBridge\Normalizer\XmlAttributesDenormalizer;
use Laminas\Config\Config;
use Symfony\Component\Serializer\SerializerInterface;
use VuFindHttp\HttpServiceInterface as HttpService;


/**
 * "Get Dbis data" AJAX Handler
 *
 * This service will retrieve the data of webservice call Datenbank-Infosystem
 * to display licenses
 *
 * @package finc\Dbis\Client
 * @author  Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 */
class DbisClient
{
    protected const URL = 'https://dbis.ur.de/dbinfo/dbliste.php';

    protected const QUERYPARAMSSUBJECT = "sort=type&lett=f&colors=255&ocolors=40&gebiete=%s&collid=&xmloutput=1";
    protected static $QUERYPARAMS;
    
    public static function initialize() {
        self::$QUERYPARAMS = "sort=type&lett=f&colors=255&ocolors=40&" .
            "gebiete=" . implode(',', range(1, 100)) .
            "&collid=&xmloutput=1";
    }

    /**
     * @var HttpService
     */
    protected $httpService;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * DbisClient constructor.
     *
     * @param HttpService             $httpService
     * @param Config                  $config
     * @param SerializerInterface     $serializer
     */
    public function __construct(
        HttpService $httpService,
        ?Config $config,
        SerializerInterface $serializer
    ) {
        self::initialize();
        $this->httpService = $httpService;
        $this->config = $config;
        $this->serializer = $serializer;
    }

    /**
     * @param $bibId
     *
     * @return DbisPage
     * @throws DbisClientException
     */
    public function fetchDbisPage($bibId): DbisPage
    {
        $additonalparams = isset($this->config->Subject->value)
            ? sprintf(self::QUERYPARAMSSUBJECT, $this->config->Subject->value)
            : self::$QUERYPARAMS;
        $client = $this->httpService->createClient(
            self::URL . "?bib_id=$bibId&" . $additonalparams
        );
        
        /* @var \Laminas\Http\Response $response */
        $response = $client->send();

        if ($response->getStatusCode() !== 200) {
            throw new DbisClientException("Unexpected response",
                $response->getStatusCode());
        }

        /** @var DbisPage $dbisPage */
        $dbisPage = $this->serializer->deserialize(
            (string)$response->getBody(), DbisPage::class, 'xml', [
            XmlAttributesDenormalizer::class => true,
            SnakeCaseObjectNormalizer::class => true,
            'xml_root_node_name'             => 'dbis_page',
        ]);

        $dbisPage->setBibId($bibId);

        return $dbisPage;
    }
}
