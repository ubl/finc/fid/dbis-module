<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php NU GPLv2
 */
namespace finc\Dbis\Client;

use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Laminas\Diactoros\RequestFactory;
use \VuFind\Config\PluginManager as PluginManager;

class DbisClientFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return DbisClient
     */
    public function __invoke(ContainerInterface $container): DbisClient
    {
        return new DbisClient(
            $container->get(\VuFindHttp\HttpService::class),
            $container->has(PluginManager::class) ? $container->get(PluginManager::class)->get('dbis') : null,
            $container->get(SerializerInterface::class)
        );
    }
}
