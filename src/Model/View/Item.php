<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Dbis\Model\View;

use finc\Dbis\Model\Api\Db;
use finc\Dbis\Model\Api\DbisPage;

/**
 * Helper Class for view
 *
 * @package  finc\Dbis\Model\View
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Item
{
    protected const URL = 'https://dbis.ur.de/dbinfo/detail.php';

    protected const QUERY = 'colors=&ocolors=&lett=f&tid=0';
    /**
     * @var DbisPage
     */
    protected $dbisPage;

    /**
     * @var Db
     */
    protected $db;

    public function __construct(DbisPage $dbisPage, Db $db)
    {
        $this->dbisPage = $dbisPage;
        $this->db = $db;
    }

    /**
     * @return Db
     */
    public function getDb(): Db
    {
        return $this->db;
    }

    public function getName(): string
    {
        return $this->db->getText();
    }

    public function getUrl(): string
    {
        $bibId = $this->dbisPage->getBibId();
        $titleId = $this->db->getTitleId();
        return self::URL . "?bib_id=$bibId&titel_id=$titleId&" . self::QUERY;
    }

    /**
     * @return string
     */
    public function getAccessText(): string
    {
        if(!isset($this->dbisPage->getListDbs()->getDbAccessInfos()->getDbAccessInfo()[$this->db->getAccessRef()]))
            return '';

        return $this->dbisPage->getListDbs()->getDbAccessInfos()->getDbAccessInfo()[$this->db->getAccessRef()]->getDbAccessShortText() ?? '';
    }
}
