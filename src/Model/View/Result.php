<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Dbis\Model\View;

use finc\Dbis\Model\Api\DbisPage;
use finc\Dbis\Model\Api\DbAccessInfos;

/**
 * Helper Class for view
 *
 * @package  finc\Dbis\Model\Api
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Result implements \IteratorAggregate
{
    /**
     * @var DbisPage
     */
    protected $dbisPage;

    public function __construct(DbisPage $dbisPage)
    {
        $this->dbisPage = $dbisPage;
    }

    /**
     * @return iterable|Group[]
     */
    public function getIterator(): iterable
    {
        $listDbs = $this->dbisPage->getListDbs();
        $types = $listDbs->getDbTypeInfos()->getDbTypeInfo();

        foreach ($types as $type) {
            yield new Group($this->dbisPage, $type);
        }
    }

    /**
     *  @return DbAccessInfos
     */
    public function getDbAccessInfos(): DbAccessInfos
    {
        $listDbs = $this->dbisPage->getListDbs();
        return $listDbs->getDbAccessInfos();
    }
}