<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Dbis\Model\Api;

/**
 * JSON Mapping Class DbAccessInfo
 *
 * @package  finc\Dbis\Model\Api
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class DbAccessInfo
{
    /**
     * @var string
     */
    protected $accessId;
    /**
     * @var string
     */
    protected $accessIcon;
    /**
     * @var string
     */
    protected $dbAccess;
    /**
     * @var string
     */
    protected $dbAccessShortText;

    /**
     * @return string
     */
    public function getAccessId(): string
    {
        return $this->accessId;
    }

    /**
     * @param string $accessId
     */
    public function setAccessId(string $accessId): void
    {
        $this->accessId = $accessId;
    }

    /**
     * @return string
     */
    public function getAccessIcon(): string
    {
        return $this->accessIcon;
    }

    /**
     * @param string $accessIcon
     */
    public function setAccessIcon(string $accessIcon): void
    {
        $this->accessIcon = $accessIcon;
    }

    /**
     * @return string
     */
    public function getDbAccess(): string
    {
        return $this->dbAccess;
    }

    /**
     * @param string $dbAccess
     */
    public function setDbAccess(string $dbAccess): void
    {
        $this->dbAccess = $dbAccess;
    }

    /**
     * @return string
     */
    public function getDbAccessShortText(): string
    {
        return $this->dbAccessShortText;
    }

    /**
     * @param string $dbAccessShortText
     */
    public function setDbAccessShortText(string $dbAccessShortText): void
    {
        $this->dbAccessShortText = $dbAccessShortText;
    }
}
