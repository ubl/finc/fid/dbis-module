<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Dbis\Model\Api;

/**
 * JSON Mapping Class DbTypeInfo
 *
 * @package  finc\Dbis\Model\Api
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class DbTypeInfo
{
    /**
     * @var string
     */
    protected $dbTypeId;
    /**
     * @var int
     */
    protected $dbt;
    /**
     * @var string
     */
    protected $dbType;
    /**
     * @var string
     */
    protected $dbTypeLongText;

    /**
     * @return string
     */
    public function getDbTypeId(): string
    {
        return $this->dbTypeId;
    }

    /**
     * @param string $dbTypeId
     */
    public function setDbTypeId(string $dbTypeId): void
    {
        $this->dbTypeId = $dbTypeId;
    }

    /**
     * @return int
     */
    public function getDbt(): int
    {
        return $this->dbt;
    }

    /**
     * @param int $dbt
     */
    public function setDbt(int $dbt): void
    {
        $this->dbt = $dbt;
    }

    /**
     * @return string
     */
    public function getDbType(): string
    {
        return $this->dbType;
    }

    /**
     * @param string $dbType
     */
    public function setDbType(string $dbType): void
    {
        $this->dbType = $dbType;
    }

    /**
     * @return string
     */
    public function getDbTypeLongText(): string
    {
        return $this->dbTypeLongText;
    }

    /**
     * @param string $dbTypeLongText
     */
    public function setDbTypeLongText(string $dbTypeLongText): void
    {
        $this->dbTypeLongText = $dbTypeLongText;
    }
}
