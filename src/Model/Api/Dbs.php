<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Dbis\Model\Api;

/**
 * JSON Mapping Class Dbs
 *
 * @package  finc\Dbis\Model\Api
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Dbs
{

    /**
     * @var int
     */
    protected $dbCount = 0;

    protected $dbTypeRef = '';
    /**
     * @var Db[]
     */
    protected $db = [];

    /**
     * @return int
     */
    public function getDbCount(): int
    {
        return $this->dbCount;
    }

    /**
     * @param int $dbCount
     */
    public function setDbCount(int $dbCount): void
    {
        $this->dbCount = $dbCount;
    }

    public function getDbTypeRef()
    {
        return $this->dbTypeRef;
    }

    public function setDbTypeRef(string $dbTypeRef)
    {
        $this->dbTypeRef = $dbTypeRef;
    }

    /**
     * @param Db $db
     */
    public function addDb(Db $db): void
    {
        $this->db[] = $db;
    }

    /**
     * @return Db[]
     */
    public function getDb(): array
    {
        return $this->db;
    }

    /**
     * @param Db[] $db
     */
    public function setDb(array $db): void
    {
        $this->db = $db;
    }
}
