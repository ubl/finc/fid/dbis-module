<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Dbis\Model\Api;

/**
 * JSON Mapping Class Db
 *
 * @package  finc\Dbis\Model\Api
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Db
{
    /**
     * @var int
     */
    protected $titleId;
    /**
     * @var string
     */
    protected $accessRef = '';

    protected $dbTypeRefs = '';
    /**
     * @var int|null
     */
    protected $topDB;
    /**
     * @var string
     */
    protected $text;

    /**
     * @return int
     */
    public function getTitleId(): int
    {
        return $this->titleId;
    }

    /**
     * @param mixed $titleId
     */
    public function setTitleId(int $titleId): void
    {
        $this->titleId = $titleId;
    }

    /**
     * @return string
     */
    public function getAccessRef(): string
    {
        return $this->accessRef;
    }

    /**
     * @param mixed $accessRef
     */
    public function setAccessRef(string $accessRef): void
    {
        $this->accessRef = $accessRef;
    }

    public function getDbTypeRefs()
    {
        return $this->dbTypeRefs;
    }

    public function setDbTypeRefs($dbTypeRefs): void
    {
        $this->dbTypeRefs = $dbTypeRefs;
    }

    /**
     * @return int|null
     */
    public function getTopDB(): ?int
    {
        return $this->topDB;
    }

    /**
     * @param int $topDB
     */
    public function setTopDB(int $topDB): void
    {
        $this->topDB = $topDB;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }
}
