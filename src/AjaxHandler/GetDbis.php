<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php NU GPLv2
 */

namespace finc\Dbis\AjaxHandler;

use finc\Dbis\Client\DbisClient;
use finc\Dbis\Client\DbisClientException;
use finc\Dbis\Model\View\Result;
use Psr\Http\Client\ClientExceptionInterface as HttpClientExceptionInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use VuFind\AjaxHandler\AbstractBase;
use Laminas\Config\Config;
use Laminas\Mvc\Controller\Plugin\Params;
use Laminas\View\Renderer\RendererInterface;

class GetDbis extends AbstractBase
{
    protected const TEMPLATE        = 'finc/dbis/result.phtml';
    protected const DEFAULT_BIBID   = 'allefreien';
    protected const EMPTY_BIBID     = 'AAAAA';

    /**
     * HTTP Service
     *
     * @var DbisClient
     */
    protected $client;

    /**
     * View renderer
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * GetDbis constructor.
     *
     * @param DbisClient        $client
     * @param RendererInterface $renderer
     *
     */
    public function __construct(DbisClient $client, RendererInterface $renderer)
    {
        $this->client = $client;
        $this->renderer = $renderer;
    }

    /**
     * @param Params $params
     *
     * @return array
     * @throws DbisClientException
     * @throws HttpClientExceptionInterface
     */
    public function handleRequest(Params $params)
    {
        $bibId = $this->getBibId($params);

        if ($this->getIsIdMappingEnabled($params)) {
            $bibId = $this->map_BibId_to_DbisId($bibId);
        }

        $result = new Result($this->client->fetchDbisPage($bibId));
        $html = $this->renderer->render(self::TEMPLATE, compact('result'));
        return $this->formatResponse(compact('html'));
    }

    /**
     * Get the library bid id
     *
     * @param Params $params
     * @return string
     */
    protected function getBibId(Params $params): string
    {
        if (($bibId = $params->fromQuery('bibId')) && strcasecmp($bibId, self::EMPTY_BIBID) != 0) {
            return strtoupper($bibId);
        }

        return self::DEFAULT_BIBID;
    }

    /**
     * @param Params $params
     * @return bool
     */
    protected function getIsIdMappingEnabled(Params $params): bool
    {
        if ($params->fromQuery('map_bibid_to_dbis')) {
            return true;
        }

        return false;
    }

    /**
     * @param $bibId
     * @return string
     *
     * @throws FileNotFoundException
     * @throws InvalidConfigurationException
     * @throws MissingOptionsException
     */
    protected function map_BibId_to_DbisId($bibId)
    {
        $configFile = "config.ini";
        $optionKey  = "bibId_exemption_mapping_list";

        if (! file_exists($config = __DIR__ . "/../../res/config/$configFile")) {
            throw new FileNotFoundException(sprintf('The file "%s" does not exist.', $configFile), 0, null, []);
        }

        if (! ($exceptions = parse_ini_file($config,true))) {
            throw new InvalidConfigurationException(sprintf('The file "%s" could not be parsed.', $configFile));
        }

        if (! isset($exceptions[$optionKey])) {
            throw new MissingOptionsException(sprintf('Missing option "%s" in file "%s".', $optionKey, $configFile));
        }

        if (array_key_exists($bibId, $exceptions[$optionKey])) {
            return $exceptions[$optionKey][$bibId];
        }

        return $bibId;
    }
}
